(function () {

    var table = document.querySelector("#myTable"),
            ths = table.querySelectorAll("thead th"),
            trs = table.querySelectorAll("tbody tr"),
            tbody = table.querySelector('tbody'),
            factor = 1,
            array = [];
    setEvent();

    function setEvent() {
        var i,
                length = ths.length;
        for (i = 0; i < length; i++) {
            ths[i].addEventListener('click', setSortBy);
        }
    }
    function previousIndex(number){
        array.push(number);
        if(array.length>2){
            array.shift();
        }
        if (ths[array[0]].getAttribute('class') !== null && array[0]!==array[1]) {
                ths[array[0]].className = '';
                factor = 1;
            }
    }
    function setSortBy(e) {
        var target = e.target;
        arrayOfths = createArray(ths);
        index = arrayOfths.indexOf(target);
        arrayOftrs = createArray(trs);
        previousIndex(index);
        if (index !== -1) {
            if (factor === 1) {
                appendNewHTMLElements(sortArray(arrayOftrs, index, factor));
                this.classList.add("asc");
                this.classList.remove("desc");
                factor = -1;
            } else {
                appendNewHTMLElements(sortArray(arrayOftrs, index, factor));
                this.classList.add("desc");
                this.classList.remove("asc");
                factor = 1;
            }
        }
    }

    function createArray(objects) {
        var i, array = [], length;
        length = objects.length;
            for (i = 0; i < length; i++) {
                array.push(objects[i]);
            }
        return array;
    }

    function sortArray(array, index, factor) {
        array.sort(function (a, b) {
            if (a.children[index].textContent > b.children[index].textContent)
                return 1 * factor;
            else if (a.children[index].textContent < b.children[index].textContent)
                return -1 * factor;
            else
                return 0;
        });
        return array;
    }
    function appendNewHTMLElements(sortedArray) {
        var length = sortedArray.length, i, part = document.createDocumentFragment();
        for (i = 0; i < length; i++) {
            part.appendChild(sortedArray[i]);
        }
        tbody.appendChild(part);
    }

})();